package com.example.joseluissanchez.marsroverkt.model

import org.junit.After
import org.junit.Before

import junit.framework.Assert.assertEquals

import org.junit.Test

/**
 * Created by jose.sanchez.olivero on 14/12/17.
 */
class OrientationTest {

    private lateinit var sut: Orientation

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun turnRigthFromNorthChangeToEast() {
        //Arrange
        var sut = Orientation.NORTH
        //Act
        val direction = sut.turnRight()
        //Assert
        assertEquals(Orientation.EAST, direction)
    }

    @Test
    fun turnRigthFromEastChangeToSouth() {
        //Arrange
        var sut = Orientation.EAST
        //Act
        val direction = sut.turnRight()
        //Assert
        assertEquals(Orientation.SOUTH, direction)
    }

    @Test
    fun turnLeftFromNorthChangeToWest() {
        //Arrange
        var sut = Orientation.NORTH
        //Act
        val direction = sut.turnLeft()
        //Assert
        assertEquals(Orientation.WEST, direction)
    }

    @Test
    fun turnLeftFromSouthChangeToEast() {
        //Arrange
        var sut = Orientation.SOUTH
        //Act
        val direction = sut.turnLeft()
        //Assert
        assertEquals(Orientation.EAST, direction)
    }
}