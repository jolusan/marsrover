package com.example.joseluissanchez.marsroverkt.model

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Ignore

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

/**
 * Created by jose.sanchez.olivero on 14/12/17.
 */
class RoverTest {

    private lateinit var sut: Rover

    val TEST_X = 1
    val TEST_Y = 3

    val TEST_DEFAULT_DIR: Char = 'N'

    @Before
    fun setUp() {
        sut = Rover()
    }

    @After
    fun tearDown() {
    }

    @Test
    fun sutIsNotNil() {
        assertNotNull(sut)
    }

    @Test
    fun initialXpositionIs_0() {
        assertEquals(0, Rover.DEFAULT_X)
    }

    @Test
    fun initialYpositionIs_0() {
        assertEquals(0, Rover.DEFAULT_Y)
    }

    @Test
    fun initialRoverisAt_DEFAULT_X() {
        assertEquals(Rover.DEFAULT_X, sut.x)
    }

    @Test
    fun initialRoverisAt_DEFAULT_Y() {
        assertEquals(Rover.DEFAULT_Y, sut.y)
    }

    @Test
    fun initRoverWithXPersist() {
        sut = Rover(TEST_X)
        assertEquals(TEST_X, sut.x)
    }

    @Test
    fun initRoverWithYPersist() {
        //Arrange
        sut = Rover(y = TEST_Y)
        //Act
        //Assert
        assertEquals(TEST_Y, sut.y)
    }
/*    Estos test iban con dirección con caracteres
    @Test
    fun initialOrientationIsN() {
        assertEquals('N', sut.direction)
    }

    @Test
    fun initRoverOrientationIsN() {
        sut = Rover()
        assertEquals(Rover.DEFAULT_DIRECTION,sut.direction)
    }

    @Test
    fun initRoverWithDirectionPersist() {
        //Arrange
        sut = Rover(direction = 'N')
        //Act
        //Assert
        assertEquals(TEST_DEFAULT_DIR,sut.direction)
    }

    @Test
    fun rotateRoverToRightModifiesDirection() {
        //Arrange
        //Act
        sut.applyCommand(command = "R")
        //Assert
        assertEquals('E',sut.direction)
    }

    @Test
    fun twoSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RR")
        //Assert
        assertEquals('S',sut.direction)
    }

    @Test
    fun threeSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RRR")
        //Assert
        assertEquals('W',sut.direction)
    }

    @Test
    fun fourSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RRRR")
        //Assert
        assertEquals('N',sut.direction)
    }

    @Test
    fun turnRightFromTestResultSouth() {
        //Arrange
        sut.direction = 'E'
        //Act
        sut.applyCommand("R")
        //Assert
        assertEquals('S',sut.direction)
    }


    @Ignore
    @Test
    fun rotateRoverToLeftModifiesDirection() {
        //Arrange
        //Act
        sut.applyCommand(command = "L")
        //Assert
        assertEquals('W',sut.direction)
    }
    */

    @Test
    fun initialOrientationIsN() {
        assertEquals(Orientation.NORTH, sut.direction)
    }

    @Test
    fun initRoverOrientationIsN() {
        sut = Rover()
        assertEquals(Rover.DEFAULT_DIRECTION, sut.direction)
    }

    @Test
    fun initRoverWithDirectionPersist() {
        //Arrange
        sut = Rover(direction = Orientation.NORTH)
        //Act
        //Assert
        assertEquals(Orientation.NORTH, sut.direction)
    }

    @Test
    fun rotateRoverToRightModifiesDirection() {
        //Arrange
        //Act
        sut.applyCommand(command = "R")
        //Assert
        assertEquals(Orientation.EAST, sut.direction)
    }

    @Test
    fun twoSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RR")
        //Assert
        assertEquals(Orientation.SOUTH, sut.direction)
    }

    @Test
    fun threeSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RRR")
        //Assert
        assertEquals(Orientation.WEST, sut.direction)
    }

    @Test
    fun fourSameMovesOnDefaultPutRoverToSouthDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "RRRR")
        //Assert
        assertEquals(Orientation.NORTH, sut.direction)
    }

    @Test
    fun turnRightFromEastResultSouth() {
        //Arrange
        sut.direction = Orientation.EAST
        //Act
        sut.applyCommand("R")
        //Assert
        assertEquals(Orientation.SOUTH, sut.direction)
    }

    @Test
    fun rotateRoverToLeftModifiesDirection() {
        //Arrange
        //Act
        sut.applyCommand(command = "L")
        //Assert
        assertEquals(Orientation.WEST, sut.direction)
    }

    @Test
    fun rotateRoverLThenRputOnDefaultDirection() {
        //Arrange
        sut = Rover()
        //Act
        sut.applyCommand(command = "LR")
        //Assert
        assertEquals(Orientation.NORTH, sut.direction)
    }

    @Test
    fun rotateRoverLThenRputOnInitDirection() {
        //Arrange
        sut.direction = Orientation.SOUTH
        //Act
        sut.applyCommand(command = "LR")
        //Assert
        assertEquals(Orientation.SOUTH, sut.direction)
    }

    @Test
    fun rightCommandCallsturnRight() {
        //Arrange
        val dirMock = OrientationMocks(Orientation.NORTH)
        //Act
        sut.direction = dirMock.turnRight()
        sut.applyCommand("R")
        //Assert
        assertTrue(dirMock.turnRightCalled)
    }

    @Ignore
    @Test
    fun turnValueIsStoredTurnRight() {
        val dirMock = OrientationMocks(Orientation.EAST)
        sut.direction = dirMock.turnRight()
        sut.applyCommand("R")
        assertTrue(dirMock.turnRightCalled)
        assertEquals(Orientation.EAST, sut.direction)
    }

    @Test
    fun goForwardFromInitialStateIncrementsY() {
        sut.applyCommand("F")
        assertEquals(0, sut.x)
        assertEquals(4, sut.y)
    }

    @Test
    fun goForwardFromNoZeroIncrementsY() {
        sut = Rover(0, 4, Orientation.NORTH)
        sut.applyCommand("F")
        assertEquals(0, sut.x)
        assertEquals(5, sut.y)
    }

    @RunWith(Parameterized::class)
    class moveForwardChangeYIfIsOrientated(val initY: Int, val orientation: Orientation, val finalY: Int) {
        var sut = Rover()
        companion object {
            @JvmStatic
            @Parameterized.Parameters
            fun changeY(): List<Array<Any>> {
                return listOf(
                        arrayOf(0, Orientation.NORTH, 2),
                        arrayOf(1, Orientation.NORTH, 2),
                        arrayOf(0, Orientation.EAST, 0),
                        arrayOf(4, Orientation.SOUTH, 3),
                        arrayOf(3, Orientation.WEST, 3)
                )
            }
        }

        @Test
        fun changeYwhenGoesForward() {
            sut = Rover(y = initY, direction = orientation)
            sut.applyCommand("F")
            assertEquals(finalY, sut.y)


        }
    }

    @RunWith(Parameterized::class)
    class moveForwardChangeXIfIsOrientated(val initX: Int, val orientation: Orientation, val finalX: Int) {
        var sut = Rover()
        companion object {
            @JvmStatic
            @Parameterized.Parameters
            fun changeX(): List<Array<Any>> {
                return listOf(
                        arrayOf(0, Orientation.NORTH, 0),
                        arrayOf(0, Orientation.EAST, 1),
                        arrayOf(3, Orientation.EAST, 4),
                        arrayOf(3, Orientation.SOUTH, 3),
                        arrayOf(3, Orientation.WEST, 2)
                )
            }
        }

        @Test
        fun changeXwhenGoesForward() {
            sut = Rover(x = initX, direction = orientation)
            sut.applyCommand("F")
            assertEquals(finalX, sut.x)
        }
    }

    @RunWith(Parameterized::class)
    class moveBackChangeXYIfIsOrientated(val initX: Int, val initY: Int, val orientation: Orientation, val finalX: Int, val finalY: Int) {
        var sut = Rover()
        companion object {
            @JvmStatic
            @Parameterized.Parameters
            fun changeX(): List<Array<Any>> {
                return listOf(
                        arrayOf(1, 1, Orientation.NORTH, 1, 0),
                        arrayOf(2, 0, Orientation.EAST, 1, 0),
                        arrayOf(3, 4, Orientation.SOUTH, 3, 5),
                        arrayOf(3, 4, Orientation.WEST, 4, 4)
                )
            }
        }

        @Test
        fun changeXYwhenGoesBack() {
            sut = Rover(x = initX, y = initY, direction = orientation)
            sut.applyCommand("B")
            assertEquals(finalX, sut.x)
            assertEquals(finalY, sut.y)
        }
    }

    class OrientationMocks(orientation: Orientation) : Directionable {

        var turnRightCalled = false
        var turnLeftCalled = false
        val expectedReturn = orientation

        override fun turnRight(): Orientation {
            turnRightCalled = true
            return expectedReturn
        }

        override fun turnLeft(): Orientation {
            turnLeftCalled = true
            return expectedReturn
        }

        override fun orientation(): Orientation {
            return expectedReturn
        }
    }
}