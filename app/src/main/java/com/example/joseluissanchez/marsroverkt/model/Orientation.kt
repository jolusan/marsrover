package com.example.joseluissanchez.marsroverkt.model

/**
 * Created by jose.sanchez.olivero on 14/12/17.
 */
enum class Orientation : Directionable {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    override fun turnRight(): Orientation {

        when (this) {
            NORTH -> return EAST
            EAST -> return SOUTH
            SOUTH -> return WEST
            WEST -> return NORTH
        }
    }

    override fun turnLeft(): Orientation {

        when (this) {
            NORTH -> return WEST
            WEST -> return SOUTH
            SOUTH -> return EAST
            EAST -> return NORTH
        }
    }

    override fun orientation(): Orientation {
        return NORTH
    }
}
