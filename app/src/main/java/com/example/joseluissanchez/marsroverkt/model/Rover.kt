package com.example.joseluissanchez.marsroverkt.model

/**
 * Created by jose.sanchez.olivero on 14/12/17.
 */
class Rover(var x: Int = DEFAULT_X, var y: Int = DEFAULT_Y, var direction: Orientation = DEFAULT_DIRECTION) {

    companion object {
        const val DEFAULT_X = 0
        const val DEFAULT_Y = 0
        val DEFAULT_DIRECTION = Orientation.NORTH
    }

    lateinit var cardinal: Directionable

    fun applyCommand(command: String) {

        for (i in 1..command.length) {

            if (command.get(i - 1).toString() == "R") {
                direction = direction.turnRight()
            } else if (command.get(i - 1).toString() == "L") {
                direction = direction.turnLeft()
            } else if (command.get(i - 1).toString() == "F") {
                when (direction) {
                    Orientation.NORTH -> y++
                    Orientation.SOUTH -> y--
                    Orientation.EAST -> x++
                    Orientation.WEST -> x--
                }
            } else if (command.get(i - 1).toString() == "B") {
                when (direction) {
                    Orientation.NORTH -> y--
                    Orientation.SOUTH -> y++
                    Orientation.EAST -> x--
                    Orientation.WEST -> x++
                }
            }
        }
    }
}
