package com.example.joseluissanchez.marsroverkt.model

/**
 * Created by jose.sanchez.olivero on 14/12/17.
 */
interface Directionable {
    fun turnRight(): Orientation
    fun turnLeft(): Orientation
    fun orientation(): Orientation
}